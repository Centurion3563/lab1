﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1_Tarchevsky
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введiть початкове значення Xmin: ");
            string sxMin = Console.ReadLine();
            double xMin = Double.Parse(sxMin);
            Console.Write("Введiть кiнцеве значення Xmax: ");
            string sxMax = Console.ReadLine();
            double xMax = double.Parse(sxMax);
            Console.Write("Введiть прирiст dX: ");
            string sdx = Console.ReadLine();
            double dx = double.Parse(sdx);
            double x = xMin;
            double x2 = x * 3;
            double midRes = 1;
            double y;
            while (x <= xMax)
            {
                y = Math.Pow((Math.Pow(x2,2)+x/x2)/(16*x*x2), 0.5);
                Console.WriteLine("x = {0}\t\t y = {1}", x, y);
                x += dx;
                if (x < xMax)
                {
                    midRes *= y;
                }
            }
            if (Math.Abs(x - xMax - dx) > 0.00000000000001)
            {
                y = Math.Pow((Math.Pow(xMax*3, 2) + xMax / (xMax*3)) / (16 * xMax * xMax*3), 0.5);
                Console.WriteLine("x = {0}\t\t y = {1}", xMax, y);
            }
            Console.WriteLine("Добуток проміжних значень = {0}", midRes);
            Console.ReadKey();
        }
    }
}
